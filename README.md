# Distrobox-Selector

Little perl script that i wrote to execute upon terminal launch and automate distrobox container entering.

# Dependencies 

- perl interpretter
- distrobox

# Usage

Append this to your .bashrc/.zshrc:

```
perl ~/{SCRIPT_LOCATION}/distroboxselector/distroboxSelector.pl
```

Or execute it uppon startup in your desired terminal emulator.

Piece of my alacritty config integrating this behavior:
```
shell:
  program: zsh
  args:
    - -c
    - perl ~/shellScripts/distroboxselector/distroboxSelector.pl
```


# Licence & Warranty

================================================================================

                             Distrobox-Selector    

            (c) 2024 Marko Stajgar <marko.stajgar@proton.me>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

================================================================================

