#!/bin/perl

use strict;
use warnings;

my $distroboxVersion = `distrobox`;

if ($? != 0) 
{
    exit 1;
}

my @distroboxList = `distrobox list`;
chomp @distroboxList;

my @distroboxNames;

for (my $i = 0; $i <= $#distroboxList; $i++)
{
    if ($i == 0)
    {
        print("   $distroboxList[$i]\n");
        next;
    }
    print("$i. $distroboxList[$i] \n")
}

foreach my $line (@distroboxList)
{
    if ($line =~ /^\s*(\S+)\s*(\S)\s*(\S+)\s*/)
    {
        push @distroboxNames, $3;
    }
}

print("\n\n");

print("Enter blank if you wish to remain in host\n");
print("Choose which distrobox you would like to enter: ");

my $userInput = <STDIN>;

if ($userInput =~ /^\s*$/)
{
    exit 0;
}
elsif ($userInput =~ /(\d+)/)
{
    my $index = $1;

    if ($index > 0 && $index <= $#distroboxNames)
    {
        print("\nGreat success! Entering: $distroboxNames[$index]\n\n");

        my @args = ("distrobox", "enter", $distroboxNames[$index]);
        system(@args);
    }
    else
    {
        print("\nInvalid index: terminating script\n");
        exit 1;
    }
}
else 
{
   print("\nInvalid input: terminating script");
   exit 1;
}
